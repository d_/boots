use crate::errors::Error;

use std::borrow::Cow;
use std::collections::HashMap;

use indexmap::IndexMap;
use regex::Regex;

/// A very rudimentary templating system using regex and string replacements.

enum TokenType {
    Required,
    Optional,
}

pub fn process(template: &str, metadata: &IndexMap<String, String>) -> Result<String, Error> {
    let mut tokens: HashMap<String, TokenType> = HashMap::new();

    let re = Regex::new(r#"\{\w+\}|\[\w+\]"#)?;

    let mut processed = Cow::from(template);
    let mut replacements = HashMap::new();

    for cap in re.captures_iter(template) {
        let token = &cap[0];
        // TODO evaluate unwrap here
        match token.chars().next().unwrap() {
            '{' => {
                tokens.insert(token.to_string(), TokenType::Required);
            }
            '[' => {
                tokens.insert(token.to_string(), TokenType::Optional);
            }
            _ => {
                unreachable!();
            }
        }
    }

    for (token, tokentype) in tokens.iter() {
        // TODO evaluate unwrap
        let value = metadata.get(token.get(1..token.len() - 1).unwrap());
        match tokentype {
            TokenType::Required => {
                let value: String = match value {
                    Some(ref value) => (*value).to_string(),
                    None => {
                        return Err(Error::TemplateError(token.to_string()));
                    }
                };
                replacements.insert(token.to_string(), value);
            }
            TokenType::Optional => {
                let value: String = value.unwrap_or(&"".to_string()).to_string();
                replacements.insert(token.to_string(), value);
            }
        };
    }

    for (from, to) in replacements.iter() {
        processed = processed.replace(from, to).into();
    }

    Ok(processed.to_string())
}
