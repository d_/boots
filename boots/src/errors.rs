use std::fmt;
use std::io;
use std::num::ParseIntError;
use std::path::PathBuf;

use config::ConfigError;
use regex::Error as RegexError;
use rusqlite::Error as RusqliteError;
use shellexpand::LookupError;

#[derive(Debug)]
pub enum Error {
    Io(io::Error, PathBuf),
    ConfigError(ConfigError),
    ShellExpandLookupError(LookupError<std::env::VarError>),
    RusqliteError(RusqliteError),
    MissingRequiredMetadata(String),
    EmptyRequiredMetadata(String),
    ChronoParseError(chrono::format::ParseError),
    Isbn13ParseError(isbn::IsbnError),
    RegexError(RegexError),
    ParseIntError(ParseIntError),
    TemplateError(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Io(e, p) => write!(f, "{:?}: {}", &p, &e),
            Error::ConfigError(e) => write!(f, "{}", &e),
            Error::ShellExpandLookupError(e) => write!(f, "{}", &e),
            Error::RusqliteError(e) => write!(f, "{}", &e),
            Error::MissingRequiredMetadata(e) => write!(f, "Missing Required Key: {}", &e),
            Error::EmptyRequiredMetadata(e) => write!(f, "Required Key lacks Value: {}", &e),
            Error::ChronoParseError(e) => write!(f, "Failed to parse date: {}", &e),
            Error::Isbn13ParseError(e) => write!(f, "Failed to parse ISBN-13: {}", &e),
            Error::RegexError(e) => write!(f, "{}", &e),
            Error::ParseIntError(e) => write!(f, "{}", &e),
            Error::TemplateError(e) => write!(f, "Unknown field in path template: {}", &e),
        }
    }
}

impl From<ConfigError> for Error {
    fn from(err: ConfigError) -> Self {
        Error::ConfigError(err)
    }
}

impl From<LookupError<std::env::VarError>> for Error {
    fn from(err: LookupError<std::env::VarError>) -> Self {
        Error::ShellExpandLookupError(err)
    }
}

impl From<RusqliteError> for Error {
    fn from(err: RusqliteError) -> Self {
        Error::RusqliteError(err)
    }
}

impl From<chrono::format::ParseError> for Error {
    fn from(err: chrono::format::ParseError) -> Self {
        Error::ChronoParseError(err)
    }
}

impl From<isbn::IsbnError> for Error {
    fn from(err: isbn::IsbnError) -> Self {
        Error::Isbn13ParseError(err)
    }
}

impl From<RegexError> for Error {
    fn from(err: RegexError) -> Self {
        Error::RegexError(err)
    }
}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Self {
        Error::ParseIntError(err)
    }
}
