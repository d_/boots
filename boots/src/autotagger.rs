use std::collections::HashMap;
use std::path::Path;

use chrono::DateTime;
use epub::doc::EpubDoc;
use pdf::file::File as PdfFile;

fn pdf<T: AsRef<Path>>(file: T) -> HashMap<String, String> {
    let fetched = HashMap::new();

    // get metadata from pdf, if fail return empty map
    let _pdf = match PdfFile::open(file) {
        Ok(pdf) => pdf,
        Err(_) => return fetched,
    };

    // let catalog = pdf.get_root();
    // let md = catalog.metadata.as_ref().unwrap();

    // dbg!(&pdf.trailer.info_dict);
    // dbg!(md);

    fetched
}

fn epub<T: AsRef<Path>>(file: T) -> HashMap<String, String> {
    let mut fetched = HashMap::new();

    // get metadata from epub, if fail return empty map
    let epub_md = match EpubDoc::new(file) {
        Ok(doc) => doc.metadata,
        Err(_) => return fetched,
    };

    // TODO better handle aliases, maybe also do fuzzy matching
    // massage the epub metadata into `md`, renaming some keys and modifying values
    for (k, v) in epub_md.iter() {
        match (k.as_ref(), v) {
            ("creator", v) => {
                fetched.insert("authors".to_string(), v.join(";"));
            }
            ("date", v) => {
                if v.len() == 1 {
                    if let Ok(v) = DateTime::parse_from_rfc3339(&v.join("")) {
                        let v = v.format("%Y").to_string();
                        fetched.insert("year".to_string(), v);
                    }
                }
            }
            ("identifier", v) => {
                for id in v.iter() {
                    // TODO write your own sane ISBN library?
                    // TODO support ISBN-10
                    if id.len() == 13 {
                        let v = match id.parse::<isbn::Isbn13>() {
                            Ok(isbn) => isbn.to_string(),
                            Err(_) => "".to_string(),
                        };
                        fetched.insert("ISBN".to_string(), v);
                    }
                }
            }
            (_, _) => {
                fetched.insert(k.to_string(), v[0].clone());
            }
        }
    }

    fetched
}

pub fn extract<T: AsRef<Path>>(keys: Vec<String>, file: T) -> HashMap<String, String> {
    let file = file.as_ref();
    // declare the variable to hold our output
    let mut metadata = HashMap::new();
    // declare the variable to hold output of fetched metadata
    let fetched: HashMap<String, String>;

    // figure out file's extension to pick appropriate backend
    if let Some(ext) = file.extension() {
        let ext = ext.to_string_lossy().to_string();
        match ext.as_ref() {
            "epub" => fetched = epub(file),
            "pdf" => fetched = pdf(file),
            _ => return metadata,
        }
        // add "format" and "added", then everything else from fetched
        for column in keys.iter() {
            let k = column.to_string();
            let v = match column.as_ref() {
                "format" => ext.to_string(),
                "added" => chrono::Utc::now().to_rfc3339(),
                _ => fetched.get(column).unwrap_or(&"".to_string()).to_string(),
            };
            metadata.insert(k, v);
        }

        metadata
    } else {
        metadata
    }
}
