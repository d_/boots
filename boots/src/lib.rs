mod autotagger;
mod errors;
mod filter;
mod template;

pub use errors::Error;
pub use filter::Filter;

use std::collections::HashMap;
use std::fs;
use std::path::{Path, PathBuf};

use indexmap::IndexMap;
use log::info;
use path_clean::PathClean;
use rusqlite::{types::Value, Connection, ToSql, NO_PARAMS};

// TODO convert Option unwraps into Errors
// TODO make better names for methods, maybe prepend every sql method with "db"

static DEFAULT_CONFIG: &str = include_str!("../default_config.toml");

type Result<T> = std::result::Result<T, Error>;

fn copy<T: AsRef<Path>, U: AsRef<Path>>(from: T, to: U) -> Result<()> {
    let to_dir = &to.as_ref().parent().unwrap();
    if !to_dir.is_dir() {
        fs::create_dir_all(to_dir).map_err(|e| Error::Io(e, to_dir.into()))?;
    }
    fs::copy(from, &to).map_err(|e| Error::Io(e, to.as_ref().into()))?;
    Ok(())
}

#[derive(Debug, Clone)]
pub struct Boot {
    library_db: String,
    library_dir: String,
    path_template: String,
    pub columns_req: Vec<String>,
    pub columns_opt: Vec<String>,
}

impl Boot {
    pub fn new<T: Into<PathBuf>>(cfg: T) -> Result<Self> {
        let cfg = Self::read_config(&cfg.into())?;

        // shellexpand to support ~ for $HOME
        let library_db = shellexpand::full(&cfg["db"])?.into_owned();
        let library_dir = shellexpand::full(&cfg["dir"])?.into_owned();
        let path_template = cfg["path_template"].clone();

        // create the sqlite db if it doesn't exist
        if !PathBuf::from(&library_db).exists() {
            Self::create(&library_db)?
        };

        // instantiate this struct earlier to use its methods to fill out the rest of the fields
        let preboot = Boot {
            library_db: library_db.clone(),
            library_dir: library_dir.clone(),
            path_template: path_template.clone(),
            columns_req: Vec::new(),
            columns_opt: Vec::new(),
        };

        let (columns_req, columns_opt) = preboot.get_columns()?;

        Ok(Boot {
            library_db,
            library_dir,
            path_template,
            columns_req,
            columns_opt,
        })
    }

    fn read_config(cfg: &PathBuf) -> Result<HashMap<String, String>> {
        if !cfg.is_file() {
            info!("Creating default config file");
            // TODO evalutate unwrap
            let cfg_dir = cfg.parent().unwrap();
            if !cfg_dir.is_dir() {
                fs::create_dir_all(cfg_dir).map_err(|e| Error::Io(e, cfg.to_owned()))?;
            }
            fs::write(cfg, DEFAULT_CONFIG).map_err(|e| Error::Io(e, cfg.to_owned()))?;
        }
        let mut settings = config::Config::default();
        settings
            .merge(config::File::from(cfg.to_owned()))?
            .merge(config::Environment::with_prefix("BOOTS"))?;
        Ok(settings.try_into::<HashMap<String, String>>()?)
    }

    // TODO don't hardcode schema
    fn create(library_db: &str) -> Result<()> {
        let conn = Connection::open(library_db)?;
        conn.execute(
            "CREATE TABLE books (
             id INTEGER NOT NULL PRIMARY KEY,
             path TEXT NOT NULL,
             format TEXT NOT NULL,
             added TEXT NOT NULL,
             authors TEXT NOT NULL,
             title TEXT NOT NULL,
             year TEXT NOT NULL,
             original_year TEXT NOT NULL,
             publisher TEXT NOT NULL,
             language TEXT NOT NULL,
             ISBN INTEGER,
             subtitle TEXT,
             description TEXT,
             series TEXT,
             category TEXT,
             edition TEXT,
             country TEXT
         );",
            NO_PARAMS,
        )?;
        Ok(())
    }

    fn connect(&self) -> Result<Connection> {
        Ok(Connection::open(self.clone().library_db)?)
    }

    pub fn columns(&self) -> Vec<String> {
        // TODO figure out if clone can be avoided here
        let mut columns = Vec::new();
        columns.extend(self.columns_req[2..].to_owned());
        columns.extend(self.columns_opt.clone());
        columns
    }

    fn get_columns(&self) -> Result<(Vec<String>, Vec<String>)> {
        let conn = self.connect()?;
        let mut table_info = conn.prepare(r#"SELECT * FROM books.pragma_table_info("books")"#)?;
        let mut columns_req = Vec::new();
        let mut columns_opt = Vec::new();
        let mut rows = table_info.query(NO_PARAMS)?;

        while let Some(row) = rows.next()? {
            let row = row;
            let name: String = row.get(1)?;
            let nonnullable: i64 = row.get(3)?;
            match nonnullable {
                0 => columns_opt.push(name),
                1 => columns_req.push(name),
                _ => unreachable!(),
            }
        }

        Ok((columns_req, columns_opt))
    }

    fn insert(&self, metadata: &IndexMap<String, String>) -> Result<()> {
        let keys: Vec<String> = metadata.keys().map(|x| x.to_string()).collect();
        let positions: Vec<String> = (1..=metadata.keys().len())
            .map(|x| format!("?{}", x.to_string()))
            .collect();

        let mut params: Vec<&dyn ToSql> = Vec::new();

        for v in metadata.values() {
            params.push(v);
        }

        let conn = self.connect()?;

        conn.execute(
            &format!(
                "INSERT INTO books ({}) VALUES ({})",
                &keys.join(", "),
                &positions.join(", ")
            ),
            params,
        )?;
        Ok(())
    }

    pub fn fetch_metadata<T: AsRef<Path>>(&self, file: T) -> HashMap<String, String> {
        autotagger::extract(self.columns(), file)
    }

    fn validate(&self, metadata: &IndexMap<String, String>) -> Result<()> {
        for k in self.columns_req[3..].iter() {
            if !metadata.contains_key(k) {
                return Err(Error::MissingRequiredMetadata(k.to_string()));
            }
            // unwrap here should be safe since there is an early return above that outputs an
            // error if the key is not present
            if metadata.get(k).unwrap().is_empty() {
                return Err(Error::EmptyRequiredMetadata(k.to_string()));
            };
        }

        // sanity checking of inputs for known keys
        // e.g. date keys should contain dates
        for (k, v) in metadata {
            match (k.as_ref(), v) {
                ("added", v) => {
                    chrono::DateTime::parse_from_rfc3339(v)?;
                }
                ("year", v) => {
                    v.parse::<i32>()?;
                }
                ("original_year", v) => {
                    v.parse::<i32>()?;
                }
                ("ISBN", v) => {
                    v.parse::<isbn::Isbn13>()?;
                }
                (_, _) => {}
            }
        }

        Ok(())
    }

    fn construct_path(&self, metadata: &IndexMap<String, String>) -> Result<PathBuf> {
        let base_dir = self.clone().library_dir;
        let path_template = self.clone().path_template;
        let mut path = base_dir;
        let relpath = template::process(&path_template, metadata)?;
        path.push_str(&relpath);
        let path = PathBuf::from(path);
        Ok(path.clean())
    }

    pub fn import<T: AsRef<Path>>(
        &self,
        file: T,
        metadata: IndexMap<String, String>,
    ) -> Result<()> {
        self.validate(&metadata)?;

        // construct path and copy file to new path
        let path = self.construct_path(&metadata)?;
        copy(file, path.clone())?;

        // add constructed path to beginning of metadata IndexMap
        let mut metadata_complete = IndexMap::new();
        metadata_complete.insert("path".to_string(), path.to_string_lossy().to_string());
        metadata_complete.extend(metadata);

        self.insert(&metadata_complete)?;

        Ok(())
    }

    fn ready_filter(&self, filter: Filter) -> String {
        filter
            .terms
            .iter()
            .map(|x| match x {
                filter::Parsed::Ready(x) => x.to_string(),
                filter::Parsed::Unready(x) => x.to_string(self.columns()),
            })
            .collect::<Vec<String>>()
            // TODO allow OR?
            .join(" AND ")
    }

    fn query(&self, filter: Filter, column: &str) -> Result<Vec<IndexMap<String, Option<String>>>> {
        let conn = self.clone().connect()?;
        let filter = self.ready_filter(filter);
        let mut stmt = conn.prepare(&format!("SELECT {} FROM books WHERE {}", column, filter))?;
        let mut rows = stmt.query(NO_PARAMS)?;

        let mut returns = Vec::new();

        // TODO figure out what is wrong down here

        while let Some(row) = rows.next()? {
            let mut metadata = IndexMap::new();
            for (count, c) in row.columns().iter().enumerate() {
                let value = row.get::<_, Value>(count).unwrap();
                let value = match value {
                    Value::Null => None,
                    Value::Blob(_v) => todo!(),
                    Value::Real(v) => Some(v.to_string()),
                    Value::Integer(v) => Some(v.to_string()),
                    Value::Text(v) => Some(v.to_string()),
                };
                metadata.insert(c.name().to_string(), value);
            }
            returns.push(metadata);
        }
        Ok(returns)
    }

    pub fn list(&self, filter: Filter) -> Result<Vec<String>> {
        let column = "path";
        Ok(self
            .query(filter, column)?
            .iter()
            // TODO atrocious unwrapping
            .map(|s| s.get(column).unwrap().clone().unwrap())
            .collect())
    }

    pub fn info(&self, filter: Filter) -> Result<Vec<IndexMap<String, Option<String>>>> {
        Ok(self.query(filter, "*")?)
    }

    // TODO genericify
    // private method for dbupdate that lacks validation and path construction
    fn rawdbupdate<T: AsRef<str>>(
        &self,
        wherearg: T,
        metadata: &IndexMap<&str, Option<&str>>,
    ) -> Result<()> {
        let conn = self.connect()?;
        let mut params: Vec<&dyn ToSql> = Vec::new();
        let mut setargs = Vec::new();

        for (count, (k, v)) in metadata.iter().enumerate() {
            setargs.push(format!("{} = ?{}", k, count + 1));
            params.push(v);
        }

        conn.execute(
            &format!(
                "UPDATE books SET {} WHERE {}",
                setargs.join(", "),
                wherearg.as_ref()
            ),
            params,
        )?;
        Ok(())
    }

    // SQL update, not Boots update
    pub fn dbupdate(&self, filter: Filter, metadata: &IndexMap<String, String>) -> Result<()> {
        let columns = self.columns();
        let filter = self.ready_filter(filter);

        self.validate(metadata)?;
        // let path = self.construct_path(metadata)?;
        // let path = path.as_os_str().to_str().unwrap();

        // missing keys are set to NULL
        let mut nullymd: IndexMap<&str, Option<&str>> = IndexMap::new();
        // nullymd.insert("path", Some(path));
        for c in &columns {
            nullymd.insert(c, None);
        }
        for (k, v) in metadata {
            nullymd.insert(k, Some(v));
        }

        self.rawdbupdate(filter, &nullymd)?;

        Ok(())
    }

    pub fn delete(&self, filter: Filter, deletefiles: bool) -> Result<()> {
        let paths: Vec<PathBuf> = self
            .list(filter.clone())?
            .iter()
            .map(PathBuf::from)
            .collect();

        let conn = self.connect()?;
        let filter = self.ready_filter(filter);
        // TODO ask if this is vulnerable to injection
        conn.execute(&format!("DELETE FROM books WHERE {}", filter), NO_PARAMS)?;

        if deletefiles {
            for path in paths {
                fs::remove_file(path.clone()).map_err(|e| Error::Io(e, path))?;
            }
        }
        Ok(())
    }

    // limited in scope until epub-rs gains write methods
    pub fn update(&self) -> Result<Vec<String>> {
        let mut removed = Vec::new();
        let paths = self.list(Filter::from(""))?;
        for path in paths {
            let path = PathBuf::from(path);
            if !path.is_file() {
                fs::remove_file(path.clone()).map_err(|e| Error::Io(e, path.clone()))?;
                removed.push(path.to_string_lossy().to_string());
            }
        }
        Ok(removed)
    }

    // update path column
    pub fn update_paths(&self) -> Result<HashMap<String, String>> {
        let mut moves = HashMap::new();
        let metadatas = self.info(Filter::from(""))?;
        let mut mds = Vec::new();
        for metadata in metadatas {
            let mut md = IndexMap::new();
            for (k, v) in metadata {
                if let Some(v) = v {
                    md.insert(k, v);
                }
            }
            mds.push(md);
        }
        for metadata in mds {
            let old_path = metadata.get("path").unwrap();
            let new_path = self.construct_path(&metadata)?;
            if PathBuf::from(old_path) != new_path {
                copy(old_path, new_path.clone())?;
                moves.insert(old_path.to_string(), new_path.to_string_lossy().to_string());
                let mut md = IndexMap::new();
                // TODO another PathBuf to str unwrap
                md.insert("path", Some(new_path.as_os_str().to_str().unwrap()));
                self.rawdbupdate(format!("path=\"{}\"", old_path), &md)?;
                fs::remove_file(old_path).map_err(|e| Error::Io(e, PathBuf::from(old_path)))?;
            }
        }
        Ok(moves)
    }
}
