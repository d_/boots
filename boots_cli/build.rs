use clap::Shell;
use std::env::var_os;

include!("src/app.rs");

fn main() {
    let mut app = build_app();
    app.gen_completions("boots", Shell::Fish, var_os("OUT_DIR").unwrap());
    // app.gen_completions("boots", Shell::Bash, var_os("OUT_DIR").unwrap());
    // app.gen_completions("boots", Shell::Zsh, var_os("OUT_DIR").unwrap());
}
