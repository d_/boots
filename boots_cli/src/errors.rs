use std::fmt;

use std::env::VarError;
use std::io;

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    VarError(VarError),
    TomlDe(toml::de::Error),
    TomlSer(toml::ser::Error),
    Boots(boots::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Io(e) => write!(f, "{}", &e),
            Error::VarError(e) => write!(f, "{}", &e),
            Error::TomlDe(e) => write!(f, "{}", &e),
            Error::TomlSer(e) => write!(f, "{}", &e),
            Error::Boots(e) => write!(f, "{}", &e),
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::Io(err)
    }
}

impl From<VarError> for Error {
    fn from(err: VarError) -> Self {
        Error::VarError(err)
    }
}

impl From<toml::de::Error> for Error {
    fn from(err: toml::de::Error) -> Self {
        Error::TomlDe(err)
    }
}

impl From<toml::ser::Error> for Error {
    fn from(err: toml::ser::Error) -> Self {
        Error::TomlSer(err)
    }
}

impl From<boots::Error> for Error {
    fn from(err: boots::Error) -> Self {
        Error::Boots(err)
    }
}
