use crate::errors::Error;

use std::collections::HashMap;
use std::env::var;
use std::io::prelude::{Read, Write};
use std::process::Command;

use indexmap::IndexMap;
use tempfile::NamedTempFile;

// TODO make more generic

fn template<T: AsRef<str>>(
    columns_req: &[String],
    columns_opt: &[String],
    metadatas: &[HashMap<String, String>],
    ids: Vec<T>,
) -> String {
    let mut template = String::new();
    for (metadata, id) in metadatas.iter().zip(ids) {
        let columns_req: Vec<String> = columns_req[2..]
            .iter()
            .map(|x| {
                let key = x;
                let value = metadata.get(x).unwrap_or(&"".to_string()).to_string();
                format!("{} = \"{}\"", key, value)
            })
            .collect();
        let columns_opt: Vec<String> = columns_opt
            .iter()
            .map(|x| {
                let key = x;
                match metadata.get(x) {
                    Some(x) => {
                        let value = metadata.get(x).unwrap_or(&"".to_string()).to_string();
                        format!("{} = \"{}\"", key, value)
                    }
                    None => format!("#{} = \"\"", key),
                }
            })
            .collect();
        template.push_str(&format!("[\"{}\"]\n", id.as_ref()));
        template.push_str(
            &format!("{}\n{}", columns_req.join("\n"), columns_opt.join("\n"))
                .trim()
                .to_string(),
        );
        template.push_str("\n\n");
    }
    template
}

// Columns are separated into required and optional to easily make the optional keys commented out
// in TOML. Ids are what will be used as TOML table names which are returned in order to easily
// distinguish between groups of metadata.
pub fn edit<T: AsRef<str>>(
    columns_req: &[String],
    columns_opt: &[String],
    metadatas: &[HashMap<String, String>],
    ids: Vec<T>,
) -> Result<IndexMap<String, IndexMap<String, String>>, Error> {
    // The template function produces the String to be written into a temp file.
    let template = template(columns_req, columns_opt, metadatas, ids);

    let mut file: NamedTempFile = tempfile::Builder::new()
        .suffix(".toml")
        .rand_bytes(5)
        .tempfile()?;
    file.write_all(template.as_bytes())?;
    let editor = var("EDITOR")?;
    let mut file2 = file.reopen()?;

    Command::new(editor).arg(&file.path()).status()?;

    let mut kvs = String::new();
    file2.read_to_string(&mut kvs)?;

    Ok(toml::from_str(&kvs)?)
}
